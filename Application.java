public class Application {
    public static void main(String[] args){
        Student student1 = new Student();
        Student student2 = new Student();
        student1.student_name = "Victor";
        student1.r_score = 35.00;
        student1.class_r_score = 25.56;

        student2.student_name = "Jeff";
        student2.r_score = 15.00;
        student2.class_r_score = 25.56;

        student1.thing();
        student2.thing();


        Student[] section4 = new Student[3];
        section4[0] = student1;
        section4[1] = student2;
        section4[2] = new Student();

        section4[2].student_name = "Dude";

        System.out.println(section4[2].student_name);
    }
}
